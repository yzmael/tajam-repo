<?php 
/**
 * @desc	If you have something to add in add_action function add it here.
 * @author	Ryan Sutana
 * @uri		http://www.sutanaryan.com/
 *
 * @package rs-theme
 */

function wpdocs_library_init() {
    $labels = array(
			'name'               => _x( 'Libraries', 'post type general name', 'rs-theme' ),
			'singular_name'      => _x( 'Library', 'post type singular name', 'rs-theme' ),
			'menu_name'          => _x( 'Libraries', 'admin menu', 'rs-theme' ),
			'name_admin_bar'     => _x( 'Library', 'add new on admin bar', 'rs-theme' ),
			'add_new'            => _x( 'Add New', 'campaign', 'rs-theme' ),
			'add_new_item'       => __( 'Add New Library', 'rs-theme' ),
			'new_item'           => __( 'New Library', 'rs-theme' ),
			'edit_item'          => __( 'Edit Library', 'rs-theme' ),
			'view_item'          => __( 'View Library', 'rs-theme' ),
			'all_items'          => __( 'All Libraries', 'rs-theme' ),
			'search_items'       => __( 'Search Libraries', 'rs-theme' ),
			'parent_item_colon'  => __( 'Parent Libraries:', 'rs-theme' ),
			'not_found'          => __( 'No Library found.', 'rs-theme' ),
			'not_found_in_trash' => __( 'No Library found in Trash.', 'rs-theme' )
    );
 
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'library' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'show_in_rest'       => true,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
        'menu_icon'          => 'dashicons-portfolio'
    );
 
    register_post_type( 'library', $args );
}
 
add_action( 'init', 'wpdocs_library_init' );