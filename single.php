<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package rs-theme
 */

get_header();

$options = get_option( 'rs_theme_theme_options' );
?>
	
	<main id="main" class="site-main" role="main">
		<div class="container">
			
			<div class="row">
				<div class="col-md-8">

					<div id="primary" class="content-area">

						<?php
						while ( have_posts() ) : the_post();

							get_template_part( 'template-parts/content', get_post_format() );
							
							the_post_navigation();

							if( 'yes' == isset( $options['enable_post_comment'] ) ) :
								// If comments are open or we have at least one comment, load up the comment template.
								if ( comments_open() || get_comments_number() ) :
									comments_template();
								endif;
							endif;

						endwhile; // End of the loop.
						?>
						
					</div><!-- #primary -->

				</div>

				<!--sidebar-->
				<?php get_sidebar(); ?>
			</div>

		</div> <!-- .container -->
	</main><!-- #main -->
<?php
get_footer();
