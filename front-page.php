<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rs-theme
 */

get_header();

// Get site settings 
$options = get_option( 'rs_theme_theme_options' );
?>
	
	<main id="main" class="site-main" role="main">
<div class="about-section ">
	<div class="history my-5">
		<div class="container">
			<div class="row">
				
				<div class="col-md-5">
					<div class="text-center">
						<img src="<?php the_field('image'); ?>" alt="">
					</div>
				</div>
				
				<div class="col-md-7">
					<h3 class="mb-4"><?php the_field('title'); ?></h3>
					<p><?php the_field('content'); ?></p>
					<p><?php the_field('content-2nd-p'); ?></p>
					<button class="slider-button">Learn More</button>
				</div>
			</div>
		</div>
	</div>

	<div class="video" style="background-image: url('<?php the_field('aboutus-image'); ?>');">
		<img class="image-repeater" src="<?php the_field('aboutus-image'); ?>" alt="">
		<div id="myModal" class="modal">
			<!-- Modal content -->
			<div class="modal-content">
				<div class="text-center">
					<span class="close">&times;</span>
						<iframe id="videoplayer" width="80%" height="480px" src="" data-srcs="https://www.youtube.com/embed/2S24-y0Ij3Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
		</div>
			<div class="video-button">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<button id="myBtn"><img src="<?php the_field('play-icon'); ?>" alt=""></button>
							<h3 class="mt-3">WATCH OUR STORY</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>
	

	<div class="expertise-section my-5">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="text-center">
						<h3 class="m-0">EXPERTISE</h3>
						<p class="my-3">Lorem ipsum dolor sit amet proin gravida nibh vel velit</p>
						<hr/>
					</div>
				</div>
			</div>
			<div class="row text-center">
				<div class="col-md-4">
					<div class="expertise-item px-4 py-4">
						<div class="expertise-item_icon" style="background-image: url('<?php the_field('web-dev-icon'); ?>'); ">
							<img src="<?php the_field('web-dev-icon'); ?>" alt="">
						</div>
						<div class="mt-4">
							<h4 class="expertise-item_title">WEB DESIGN & DEVELOPMENT</h4>
							<p class="expertise-item_excerpt mt-3 mb-0">This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet Aenean</p>
						</div>
					</div>
					
					
				</div>
				<div class="col-md-4">
					<div class="expertise-item px-4 py-4">
						<div class="expertise-item_icon" style="background-image: url('<?php the_field('brush-icon'); ?>'); ">
							<img src="<?php the_field('brush-icon'); ?>" alt="">
						</div>
						
						<div class="mt-4">
							<h4 class="expertise-item_title">BRANDING IDENTITY</h4>
							<p class="expertise-item_excerpt mt-3 mb-0">This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet Aenean</p>
						</div>
					</div>
					
				</div>
				<div class="col-md-4">
					<div class="expertise-item px-4 py-4">
						<div class="expertise-item_icon" style="background-image: url('<?php the_field('mobile-icon'); ?>'); ">
							<img src="<?php the_field('mobile-icon'); ?>" alt="">
						</div>
						
						<div class="mt-4">
							<h4 class="expertise-item_title">MOBILE APP</h4>
							<p class="expertise-item_excerpt mt-3 mb-0">This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet Aenean</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row text-center">
				<div class="col-md-4">
					<div class="expertise-item px-4 py-4">
						<div class="expertise-item_icon" style="background-image: url('<?php the_field('pie-icon'); ?>'); ">
							<img src="<?php the_field('pie-icon'); ?>" alt="">
						</div>
						
						<div class="mt-4">
							<h4 class="expertise-item_title">SEARCH ENGINE OPTIMIZATION</h4>
							<p class="expertise-item_excerpt mt-3 mb-0">This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet Aenean</p>
						</div>
					</div>
					
				</div>
				<div class="col-md-4">
					<div class="expertise-item px-4 py-4">
						<div class="expertise-item_icon" style="background-image: url('<?php the_field('game-dev-icon'); ?>');">
							<img src="<?php the_field('game-dev-icon'); ?>" alt="">
						</div>
						
						<div class="mt-4">
							<h4 class="expertise-item_title">GAME DEVELOPMENT</h4>
							<p class="expertise-item_excerpt mt-3 mb-0">This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet Aenean</p>
						</div>
					</div>
					
					
				</div>
				<div class="col-md-4">
					<div class="expertise-item px-4 py-4">
						<div class="expertise-item_icon" style="background-image: url('<?php the_field('heart-icon'); ?>');">
							<img src="<?php the_field('heart-icon'); ?>" alt="">
						</div>
					
						<div class="mt-4">
							<h4 class="expertise-item_title">MADE WITH LOVE</h4>
							<p class="expertise-item_excerpt mt-3 mb-0">This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet Aenean</p>
						</div>
					</div>
					
					
				</div>
			</div>
		</div>
	</div>

	<div class="team-section py-5">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="text-center content">
						<h3 class="team-main_title">MEET OUR AMAZING TEAM</h3>
						<p class="team-main_sub_title">Lorem ipsum dolor sit amet proin gravida nibh vel velit</p>
						<hr/>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="team">
						<div class="team-image" style="background-image: url('<?php the_field('image-profile') ?>');">
							<img src="<?php the_field('image-profile') ?>" alt="">
						</div>
						<div class="text-center mt-4">
							<h5 class="team-item_title">SIR RYAN</h5>
							<p class="team-item_excerpt">CEO & FOUNDER</p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="team">
						<div class="team-image" style="background-image: url('<?php the_field('image-profile') ?>');">
							<img src="<?php the_field('image-profile') ?>" alt="">
						</div>
						<div class="text-center mt-4">
							<h5 class="team-item_title">MARVIN</h5>
							<p class="team-item_excerpt">ENGINEERING</p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="team">
						<div class="team-image" style="background-image: url('<?php the_field('image-profile') ?>');">
							<img src="<?php the_field('image-profile') ?>" alt="">
						</div>
						<div class="text-center mt-4">
							<h5 class="team-item_title">ROLAN</h5>
							<p class="team-item_excerpt">DESIGNER</p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="team">
						<div class="team-image" style="background-image: url('<?php the_field('image-profile') ?>');">
							<img src="<?php the_field('image-profile') ?>" alt="">
						</div>
						<div class="text-center mt-4">
							<h5 class="team-item_title">KLEVIN</h5>
							<p class=team-item_excerpt">MARKETING</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="text-center mt-5 sub-content">
						<p class="team-item_excerpt">Become part of our dream team, let’s join us !</p>
						<button class="slider-button">WE ARE HIRING</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	

<div class="works-section my-5">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<div class="text-center">
					<h3 class="works-item_title">OUR WORKS</h3>
				</div>
			</div>
			<div class="col-md-5">
				<div class="text-right">
					<p class="works-item_excerpt">See All Project in dribbble ></p>
				</div>
			</div>
		</div>
	</div>

	<div class="library mt-4">
		<?php get_template_part( 'template-parts/library-content', get_post_format() ); ?>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="text-center">
					<button class="slider-button mx-auto mt-5" id="loadMore">Load More</button>
				</div>
			</div>
		</div>
	</div>	
</div>

<div class="testimonial-section py-5" style="background-image: url('<?php the_field('testimonial-background'); ?>');">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="slider-for">
					<div class="testimonial">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<div class="text-center">
										<span class="testimonial-item_icon"><i class="fas fa-quote-left"></i></span>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div class="text-center my-5">
										<h2 class="testimonial-item_title">This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.</h2>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div class="text-center mb-4">
										<h3 class="testimonial-item_subtitle">YZMAEL</h3>
										<p class="testimonial-item_excerpt">WEB DEVELOPER</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="testimonial">
						<div class="container">
							<div class="row">
								<div class="col-lg-12">
									<div class="text-center">
										<span class="testimonial-item_icon"><i class="fas fa-quote-left"></i></span>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div class="text-center my-5">
										<h2 class="testimonial-item_title">This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.</h2>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div class="text-center mb-4">
										<h3 class="testimonial-item_subtitle">YZMAEL</h3>
										<p class="testimonial-item_excerpt">WEB DEVELOPER</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="testimonial">
						<div class="container">
							<div class="row">
								<div class="col-lg-12">
									<div class="text-center">
										<span class="testimonial-item_icon"><i class="fas fa-quote-left"></i></span>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div class="text-center my-5">
										<h2 class="testimonial-item_title">This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.</h2>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div class="text-center mb-4">
										<h3 class="testimonial-item_subtitle">YZMAEL</h3>
										<p class="testimonial-item_excerpt">WEB DEVELOPER</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="testimonial">
						<div class="container">
							<div class="row">
								<div class="col-lg-12">
									<div class="text-center">
										<span class="testimonial-item_icon"><i class="fas fa-quote-left"></i></span>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div class="text-center my-5">
										<h2 class="testimonial-item_title">This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.</h2>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div class="text-center mb-4">
										<h3 class="testimonial-item_subtitle">YZMAEL</h3>
										<p class="testimonial-item_excerpt">WEB DEVELOPER</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="testimonial">
						<div class="container">
							<div class="row">
								<div class="col-lg-12">
									<div class="text-center">
										<span class="testimonial-item_icon"><i class="fas fa-quote-left"></i></span>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div class="text-center my-5">
										<h2 class="testimonial-item_title">This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.</h2>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div class="text-center mb-4">
										<h3 class="testimonial-item_subtitle">YZMAEL</h3>
										<p class="testimonial-item_excerpt">WEB DEVELOPER</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="testimonial">
						<div class="container">
							<div class="row">
								<div class="col-lg-12">
									<div class="text-center">
										<span class="testimonial-item_icon"><i class="fas fa-quote-left"></i></span>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div class="text-center my-5">
										<h2 class="testimonial-item_title">This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.</h2>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div class="text-center mb-4">
										<h3 class="testimonial-item_subtitle">YZMAEL</h3>
										<p class="testimonial-item_excerpt">WEB DEVELOPER</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-5 mx-auto">
						<div class="slider-nav">
							<div class="img-thumbnails" style="background-image: url('<?php the_field('image-1'); ?>');"></div>
							<div class="img-thumbnails" style="background-image: url('<?php the_field('image-2'); ?>');"></div>
							<div class="img-thumbnails" style="background-image: url('<?php the_field('image-1'); ?>');"></div>
							<div class="img-thumbnails" style="background-image: url('<?php the_field('image-2'); ?>');"></div>
							<div class="img-thumbnails" style="background-image: url('<?php the_field('image-1'); ?>');"></div>
							<div class="img-thumbnails" style="background-image: url('<?php the_field('image-2'); ?>');"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="contact-section mb-5">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h3 class="my-5">GIVE US A GOOD NEWS</h3>

				<input class="name mb-4" type="text" placeholder="Name">
				<input class="email mb-4" type="text" placeholder="Email">
				<input class="subject mb-4" type="text" placeholder="Subject">
				<textarea class="message" type="text" placeholder="Your Message"></textarea>
				<button class="slider-button mt-3">Submmit</button>
			</div>
			<div class="col-md-5 offset-md-1">
				<h3 class="mt-5">OUR HAPPY CLIENT</h3>
				<div class="row">
					<div class="grid-container">
						<div class="grid-item"><img src="<?php the_field('zara-logo'); ?>" alt="#"></div>
						<div class="grid-item"><img src="<?php the_field('mango-logo'); ?>" alt="#"></div>
						<div class="grid-item"><img src="<?php the_field('barneys-logo'); ?>" alt="#"></div>  
						<div class="grid-item"><img src="<?php the_field('gucci-logo'); ?>" alt="#"></div>
						<div class="grid-item"><img src="<?php the_field('calvin-logo'); ?>" alt="#"></div>
						<div class="grid-item"><img src="<?php the_field('nike-logo'); ?>" alt="#"></div>  
						<div class="grid-item"><img src="<?php the_field('converse-logo'); ?>" alt="#"></div>
						<div class="grid-item"><img src="<?php the_field('puma-logo'); ?>" alt="#"></div>
						<div class="grid-item"><img src="<?php the_field('levis-logo'); ?>" alt="#"></div>  
						<div class="grid-item"><img src="<?php the_field('billabong-logo'); ?>" alt="#"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

	</main><!-- #main -->
<?php
get_footer();
