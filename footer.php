<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package rs-theme
 */

$options = get_option( 'rs_theme_theme_options' );
?>

	</div><!-- #content -->
	
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="site-branding">
						<?php
						if ( is_front_page() && is_home() ) { ?>
							<h1 class="site-title mb-0">

								<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
									<?php if( isset( $options['logo'] ) ) { ?>

										<img class="d-block" src="<?php echo esc_url( $options['logo'] ); ?>" alt="<?php bloginfo('name') ?>" />

									<?php } else { ?>

										<?php bloginfo( 'name' ); ?>
										
									<?php } ?>
								</a>

							</h1>
						<?php } else { ?>
							<p class="site-title mb-0">
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
									<?php if( isset( $options['logo'] ) ) { ?>

										<img src="<?php echo esc_url( $options['logo'] ); ?>" alt="<?php bloginfo('name') ?>" />

									<?php } else { ?>

										<?php bloginfo( 'name' ); ?>

									<?php } ?>
								</a>
							</p>
						<?php
						}

						?>
						
					</div><!-- .site-branding -->	
					<div class="title-content">
						<?php
							if ( is_active_sidebar( 'first-footer' ) ) :
						        dynamic_sidebar( 'first-footer' );
							 endif;
						?>
					</div>
					
				</div>
				<div class="col-md-4">
					<h4 class="mt-3 mb-4">OUR STUDIO</h4>
					<?php
							if ( is_active_sidebar( 'second-footer' ) ) :
						        dynamic_sidebar( 'second-footer' );
							 endif;
							 ?>
					
				</div>
				<div class="col-md-4">
					<h4 class="mt-3 mb-4">STAY IN TOUCH</h4>
					<input class="input-area" type="text" placeholder="Subscribe our newsletter"><button class="input-button" type="button"><img src="<?php the_field('button-img'); ?>" alt=""></button>
					<?php
							if ( is_active_sidebar( 'third-footer' ) ) :
						        dynamic_sidebar( 'third-footer' );
							 endif;
					 ?>
					
				</div>
			</div>
			<div class="row mt-4">
				<div class="col-md-8 col-lg-9">
					<?php wp_nav_menu( array( 
							'theme_location' => 'footer',
							'menu_id' 		 => 'footer-menu',
							'menu_class' 	 => 'secondary-menu',
							'container' 	 => 'ul'
						) ); 
					?>
				</div>
				<div class="col-md-4 col-lg-3">
					<p class="copyright-text">
						<?php
						if( isset( $options['footer_text'] ) ) {
							echo $options['footer_text'];
						}
					?>
					</p>
					
				</div>
			</div>
		</div>

		
	</footer><!-- #colophon -->
</div><!-- #page -->

<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900" rel="stylesheet"> 
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

<?php
	// include WP footer for hook and styles
	wp_footer();

	// display tracking or analytics code
	if( isset( $options['analytics'] ) ) {
		echo $options['analytics'];
	}
?>



</body>
</html>
