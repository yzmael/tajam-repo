<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rs-theme
 */

?>

			<?php 
			$args = array( 'post_type' => 'library', 'posts_per_page' => -1, 'order' =>'ASC' );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post();
				if ( has_post_thumbnail() ) { 
					$feat_image_url = wp_get_attachment_url( get_post_thumbnail_id() ); ?>
				<a href="<?php echo get_permalink(); ?>">
					<div class="library-image" style="background: url(<?php echo $feat_image_url; ?>) center center no-repeat; background-size: cover;">
			            <img class="repeater_hero_image" src="<?php echo $feat_image_url; ?>" alt="">
					</div>
				</a>
				<?php
				} 
				else {}
			endwhile; wp_reset_query(); 
		?>