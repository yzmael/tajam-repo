<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rs-theme
 */

get_header();

// Get site settings 
$options = get_option( 'rs_theme_theme_options' );
?>

	<main id="main" class="site-main" role="main">
		<div class="container">
			
			<div class="row">

				<div class="col-md-8">
					<div id="primary" class="content-area">
						
						<?php
							while ( have_posts() ) : the_post();

								get_template_part( 'template-parts/content', 'page' );

								if( 'yes' == isset( $options['enable_page_comment'] ) ) :
									// If comments are open or we have at least one comment, load up the comment template.
									if ( comments_open() || get_comments_number() ) :
										comments_template();
									endif;
								endif;

							endwhile; // End of the loop.
						?>
						
					</div><!-- #primary -->
				</div>

				<!--sidebar-->
				<?php get_sidebar(); ?>
				
			</div>

		</div> <!-- .container -->
	</main><!-- #main -->
<?php
get_footer();
