<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package rs-theme
 */

$options = get_option( 'rs_theme_theme_options' );
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link href="https://fonts.googleapis.com/css?family=Roboto:900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Hammersmith+One" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


<?php
wp_head();

// display tracking or analytics code
if( isset( $options['header_analytics'] ) ) {
	echo $options['header_analytics'];
}
?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'rs-theme' ); ?></a>
	
	<?php
	$masthead_style = '';
	if( get_header_image() ) {
		$masthead_style = 'background-image: url('. get_header_image() .') no-repeat 0 0; background-size: cover';
	}
	?>
	<header id="masthead" class="site-header" role="banner" style="<?php echo $masthead_style; ?>">
		<div class="header-nav">
			<div class="container">
					<div class="row align-items-center">
					
						<div class=" col-lg-12">

							<div class="site-branding">
								<?php
								if ( is_front_page() && is_home() ) { ?>
									<h1 class="site-title mb-0">

										<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
											<?php if( isset( $options['logo'] ) ) { ?>

												<img class="d-block" src="<?php echo esc_url( $options['logo'] ); ?>" alt="<?php bloginfo('name') ?>" />

											<?php } else { ?>

												<?php bloginfo( 'name' ); ?>
												
											<?php } ?>
										</a>

									</h1>
								<?php } else { ?>
									<p class="site-title mb-0">
										<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
											<?php if( isset( $options['logo'] ) ) { ?>

												<img src="<?php echo esc_url( $options['logo'] ); ?>" alt="<?php bloginfo('name') ?>" />

											<?php } else { ?>

												<?php bloginfo( 'name' ); ?>

											<?php } ?>
										</a>
									</p>
								<?php
								}

								$description = get_bloginfo( 'description', 'display' );
								if ( $description || is_customize_preview() ) : ?>
									<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
								<?php
								endif; ?>
							</div><!-- .site-branding -->
							
						<nav id="site-navigation" class="main-navigation text-center" role="navigation">
							<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
								<i class="fa fa-bars" aria-hidden="true"></i>
							</button>
							
							<?php wp_nav_menu( array( 
								'theme_location' => 'primary',
								'menu_id' 		 => 'primary-menu',
								'menu_class' 	 => 'primary-menu',
								'container' 	 => 'ul'
							) ); ?>
						</nav>
					</div>
					
				</div>
				</div>
			</div>
		</div>
	
	<div class="rslides_container">
      <ul class="rslides" id="slider2">
		<li style="background-image: url('<?php the_field('image-1'); ?>'); ">
			<img src="<?php the_field('image-1'); ?>" alt="">
			<div class="header-content">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="text-center">
								<h1>We Are Awesome Creative Agency</h1>
								<hr class="my-4"/>
								<p class="mb-5">This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.</p>
								<button class="slider-button">Learn More</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</li>
		<li style="background-image: url('<?php the_field('image-2'); ?>');">
			<img src="<?php the_field('image-2'); ?>" alt="">
			<div class="header-content">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="text-center">
								<h1>What is Lorem Ipsum?</h1>
								<hr class="my-4"/>
								<p class="mb-5">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
								<button class="slider-button">Learn More</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</li>
		<li style="background-image: url('<?php the_field('image-1'); ?>');">
			<img src="<?php the_field('image-1'); ?>" alt="">
			<div class="header-content">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="text-center">
								<h1>Why do we use it?</h1>
								<hr class="my-4"/>
								<p class="mb-5">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
								<button class="slider-button">Learn More</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</li>
		<li style="background-image: url('<?php the_field('image-2'); ?>');">
			<img src="<?php the_field('image-2'); ?>" alt="">
			<div class="header-content">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="text-center">
								<h1>Where does it come from?</h1>
								<hr class="my-4"/>
								<p class="mb-5">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia.</p>
								<button class="slider-button">Learn More</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</li>
      </ul>
      
    </div>
	</header><!-- #masthead -->






	<div id="content" class="site-content">
