/**
 * Customize WP Login page
 */

(function($) {

	/* Add login form text placeholder */
	$('#loginform input[type="text"]').attr('placeholder', 'Username');
	$('#loginform input[type="password"]').attr('placeholder', 'Password');

})(jQuery);