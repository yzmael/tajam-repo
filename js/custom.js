    $(document).ready(function(){

    	//slick
		$('.slider-for').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			asNavFor: '.slider-nav'
		});
		$('.slider-nav').slick({
			slidesToShow: 5,
			slidesToScroll: 1,
			asNavFor: '.slider-for',
			centerPadding: '60px',
			focusOnSelect: true,
			centerMode: true,
			variableWidth: true
		});

	      // Slider
      $("#slider2").responsiveSlides({
        auto: false,
        speed: 500,
        pause: false,
        pager: true,
        nav: true,
        speed: 500,
        namespace: "transparent-btns"
      });

		var mq = window.matchMedia( "(max-width: 570px)" );

			if (mq.matches) {
				$(".library-image").slice(0, 6).show();
			}
			else {
				$(".library-image").slice(0, 12).show();
			}
			
      	$("#loadMore").on('click', function (e) {
			e.preventDefault();

			$(".library-image:hidden").slice(0, 6).slideDown();
			if ($(".library-image:hidden").length == 0) {
				$("#loadMore").hide();
			}
		});

	$('#myBtn').click(function() {
		$('#myModal').show();
		document.getElementById("videoplayer").src = $('#videoplayer').attr('data-srcs');
	});

	$('.close').click(function() {
		$('#myModal').hide();
		document.getElementById("videoplayer").src = "";
	});

	var modal = document.getElementById('myModal');
	window.onclick = function(event) {
	  if (event.target == modal) {
	    $('#myModal').hide();
	    document.getElementById("videoplayer").src = "";
	  }
	}



    });